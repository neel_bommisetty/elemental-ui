import React from 'react';
import ReactDOM from 'react-dom';

import { Button, Modal,ModalHeader,ModalBody, ModalFooter,Spinner,Form,FormField, FormInput,Checkbox,Row,Col } from 'elemental'

var Elements = React.createClass({
    displayName: 'Elements',
    render() {
        return (
            <div className="app">
                <FormWrap />
                <ModalWrap />
            </div>
        );
    }
});
var FormWrap = React.createClass({
    displayName: 'Form',
    getInitialState() {
        return {
            clicked:false
        };
    },
    toggleClick(){
        this.setState({
            clicked: !this.state.clicked
        });
    },
    render() {
        return (
            <Form className="form">
                <Row>
                    <Col sm="1" md="2/3" lg="1/2">
                        <FormField label="Email address" htmlFor="basic-form-input-email">
                            <FormInput autoFocus type="email" placeholder="Enter email" name="basic-form-input-email" />
                        </FormField>
                    </Col>
                </Row>
                <Row>
                    <Col sm="1" md="2/3" lg="1/2">
                        <FormField label="Password" htmlFor="basic-form-input-password">
                            <FormInput type="password" placeholder="Password" name="basic-form-input-password" />
                        </FormField>
                    </Col>
                </Row>
                {(()=>{
                    if(this.state.clicked){
                       return  <Button type="primary" onClick={this.toggleClick}><Spinner type="inverted" />Submitting</Button>
                    }
                    else{
                      return  <Button type="primary" onClick={this.toggleClick} > Submit </Button>
                    }
                })()}

            </Form>
        );
    }
});

var ModalWrap = React.createClass({
    displayName: 'ModalWrap',
    getInitialState() {
        return {
            modalIsOpen :false
        };
    },
    toggleModal(){
        this.setState({modalIsOpen: !this.state.modalIsOpen})
    },
    render() {
        return (
                <div>
                <Button onClick={this.toggleModal}>Launch Modal</Button>
                <Modal isOpen={this.state.modalIsOpen} onCancel={this.toggleModal} backdropClosesModal>
                    <ModalHeader text="Lots of text to show scroll behavior" showCloseButton onClose={this.toggleModal} />
                    <ModalBody>[...]</ModalBody>
                    <ModalFooter>
                        <Button type="primary" onClick={this.toggleModal}>Close modal</Button>
                        <Button type="link-cancel" onClick={this.toggleModal}>Also closes modal</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
});

ReactDOM.render(
 <Elements />,
  document.getElementById('app')
);
